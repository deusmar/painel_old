let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
// .sass('resources/assets/sass/app.scss', 'public/css');
/** Utilizar este trecho apenas se os arquivos da pasta resources/assets se corromperem **/
//.copyDirectory('vendor/bower_components/gentelella/build', 'resources/assets')
    .styles(
        [
            'vendor/bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.css',
            'vendor/bower_components/gentelella/vendors/font-awesome/css/font-awesome.css',
            'resources/assets/css/custom.css',
        ], 'public/assets/css/app.css'
    )
    .scripts(
        [
            'vendor/bower_components/gentelella/vendors/jquery/dist/jquery.js',
            'vendor/bower_components/gentelella/vendors/bootstrap/dist/js/bootstrap.js',
            'vendor/bower_components/gentelella/build/js/custom.js',
            // 'resources/assets/js/custom.js',
        ], 'public/assets/js/general.js'
    )
    /* DataTables.net */
    .styles(
        [
            'vendor/bower_components/datatables.net-bs/css/dataTables.bootstrap.css',
            // 'vendor/bower_components/datatables.net-buttons-bs/css/buttons.bootstrap.css',
            'vendor/bower_components/datatables.net-responsive-bs/css/responsive.bootstrap.css',
        ], 'public/assets/css/datatables.css'
    )
    .scripts(
        [
            'vendor/bower_components/datatables.net/js/jquery.dataTables.js',
            'vendor/bower_components/datatables.net-bs/js/dataTables.bootstrap.js',
            'vendor/bower_components/datatables.net-responsive/js/dataTables.responsive.js',
            'vendor/bower_components/datatables.net-responsive-bs/js/responsive.bootstrap.js',
            // 'vendor/bower_components/datatables.net-buttons-bs/js/buttons.bootstrap.js',
            // 'vendor/bower_components/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.js',
            // 'vendor/bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.flash.js',
            // 'vendor/bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.html5.js',
            // 'vendor/bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.print.js',

        ], 'public/assets/js/datatables.js'
    )

    /* Forms */
    .styles(
        [
            'vendor/bower_components/gentelella/vendors/select2/dist/css/select2.css'
        ],
        'public/assets/css/forms.css'
    )

    .scripts(
        [
            'vendor/bower_components/gentelella/vendors/select2/dist/js/select2.js',
            'vendor/bower_components/gentelella/vendors/select2/dist/js/i18n/pt-BR.js'
        ],
        'public/assets/js/forms.js'
    )
    .copyDirectory('vendor/bower_components/gentelella/vendors/font-awesome/fonts', 'public/assets/fonts')
    .copyDirectory('vendor/bower_components/gentelella/vendors/bootstrap/fonts', 'public/assets/fonts')
    .copyDirectory('vendor/bower_components/gentelella/production/images', 'public/assets/images')
    .copy('resources/assets/datatables/Portuguese-Brasil.json', 'public/assets/datatables/Portuguese-Brasil.json');
