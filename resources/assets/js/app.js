require('./bootstrap');
// import Vue from 'vue';
window.Vue = require('vue');

import VueRouter from 'vue-router';


Vue.component('app', require('./components/ProfissionalRegistros.vue'));
Vue.component('profissional-registros', require('./components/ProfissionalRegistros.vue'));
Vue.component('example', require('./components/Example.vue'));
Vue.component('contatos', require('./components/shared/contato/Contato.vue'));
Vue.component('estabelecimento', require('./components/estabelecimento/Estabelecimento.vue'));
import App from '../components/App';
import Prestador from '../components/prestador/Prestador.vue';
import Example from './components/Example.vue';

Vue.use(VueRouter);

export var router = new VueRouter({
    routes: [
        {
            path: '/prestadores',
            name: 'prestadores',
            component: Prestador
        },
        // {
        //     path: '/',
        //     name: 'home',
        //     component: Example
        // }
    ]
});
const app = new Vue({
    el: '#app',
    router,
    render: app => app(App),
    urlm: window.location.origin
});
