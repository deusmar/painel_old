<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Painel | @yield('title')</title>

    <!-- Bootstrap + Font Awesome + Custom Theme Style -->
    <link href="{{ asset("assets/css/app.css") }}" rel="stylesheet">
    @stack('stylesheets')

</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        @include('includes.sidebar')

        @include('includes.topbar')
        <div id="app">
            @yield('main_container')
        </div>

        @include('includes.footer')

    </div>
</div>
<!-- jQuery + Bootstrap + Custom Theme Scripts -->
<script src="{{ asset("assets/js/general.js")}}"></script>
<script src="{{ asset("js/app.js")}}"></script>
@stack('scripts')

{{--<script src="//cdn.datatables.net/v/bs/dt-1.10.13/datatables.min.js"></script>--}}
<!-- Data Tables -->
{{--<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>--}}
{{--<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>--}}
{{--<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>--}}
{{--<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>--}}
{{--<script src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.31/pdfmake.min.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.31/vfs_fonts.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>--}}

</body>
</html>