@extends('layouts.blank')
@section('title',$profissional->nome)
@section('main_container')
    <div class="right_col" role="main" style="min-height: 1164px;">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Profissional</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{$profissional->nome}}</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-md-9 col-sm-8 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-4 col-xs-12">
                                            <dl>
                                                <dt>CPF</dt>
                                                <dd>
                                                    @if($profissional->cpf)
                                                        {{$profissional->cpf}}
                                                    @else
                                                        Não informado
                                                    @endif
                                                </dd>
                                            </dl>
                                        </div>
                                        <div class="col-md-4 col-xs-12">
                                            <dl>
                                                <dt>Sexo</dt>
                                                <dd>
                                                    @if($profissional->sexo == 'f')
                                                        Feminino
                                                    @else
                                                        Masculino
                                                    @endif
                                                </dd>
                                            </dl>
                                        </div>
                                        <div class="col-md-4 col-xs-12">
                                            <dl>
                                                <dt>Data de Nascimento</dt>
                                                <dd>
                                                    @if($profissional->dt_nasc)
                                                        {{$profissional->dt_nasc->format('d/m/Y')}}
                                                    @else
                                                        Não informado
                                                    @endif
                                                </dd>
                                            </dl>
                                        </div>
                                        <div class="col-xs-12">
                                            <dl>
                                                <dt>Sobre</dt>

                                                <dd>{!! nl2br(e($profissional->sobre)) !!}</dd>
                                            </dl>
                                        </div>

                                        {{--Especialidades--}}
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <h2 class="page-header">Especialidades</h2>
                                            @foreach($profissional->especialidades as $especialidade)
                                                <span class="label label-primary">{{$especialidade->nome}}</span>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-8 col-xs-offset-2">
                                            <!-- Current avatar -->
                                            @forelse($profissional->avatar as $avatar)
                                                <a href="#" data-toggle="modal" data-target="#avatarModal">
                                                    <img class="img-rounded center-block img-thumbnail"
                                                         src="{{asset('/storage/images/avatars/'.$avatar->caminho)}}"
                                                         title="{{$profissional->nome}} - Atualizada em {{$avatar->updated_at->format('d/m/Y H:i')}}"/>
                                                </a>
                                                <!-- Modal -->
                                                <div id="avatarModal" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">
                                                                    &times;
                                                                </button>
                                                                <h4 class="modal-title">Foto do Perfil</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <img class="img-rounded center-block img-thumbnail"
                                                                     src="{{asset('/storage/images/avatars/'.$avatar->caminho)}}"
                                                                     title="{{$profissional->nome}} - Atualizada em {{$avatar->updated_at->format('d/m/Y H:i')}}"/>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Fechar
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            @empty
                                                <img class="img-rounded center-block img-thumbnail"
                                                     src="{{asset('/assets/images/user.png')}}"
                                                     title="Foto Inexistente"/>
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="ln_solid"></div>
                                <a href="{{route('profissionais.index')}}" class="btn btn-default btn-sm">
                                    <i class="fa fa-arrow-left"></i>
                                    Voltar
                                </a>
                                <a href="{{route('profissionais.edit',$profissional->id)}}"
                                   class="btn btn-warning btn-sm">
                                    <i class="fa fa-pencil"></i>
                                    Editar
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    {{--Rede Credenciada--}}
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Rede Credenciada</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <table class="table table-striped">
                                @if(count($profissional->unidades)>0)
                                    <thead>
                                    <tr>
                                        <th>Estabelecimento</th>
                                        <th>Unidade</th>
                                    </tr>
                                    </thead>
                                @endif
                                <tbody>
                                {{--TODO adicionar botões para editar Unidade--}}
                                @forelse($profissional->unidades as $unidade)
                                    <tr>
                                        <td>
                                            <a href="{{route('estabelecimentos.show',$unidade->estabelecimento->id)}}"
                                               target="_blank">
                                                {{$unidade->estabelecimento->nome_fan}}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{route('unidades.show',$unidade->id)}}" target="_blank">
                                                {{$unidade->nome}}
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3">
                                            Não há vínculos
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                {{--Conselhos--}}
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Conselhos</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            @foreach($profissional->credenciais as $credencial)
                                <span class="label label-success">{{$conselhos[$credencial->conselho_id]}}
                                    : {{$credencial->registro}} - {{$credencial->uf}}</span>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop