@extends('layouts.blank')
@section('title', 'Adicionar Profissional')
@section('main_container')
    <div class="right_col" role="main" style="min-height: 1164px;">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Profissional - Adicionar</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Dados Pessoais</h2>
                            <div class="clearfix"></div>
                        </div>

                        <div class="x_content">
                            <div class="row">
                                {{-- FORM --}}
                                {{Form::open(['route' => 'profissionais.store','class'=>'form-horizontal form-label-left','files' => true, 'method'=>'post'])}}

                                <div class="col-md-9 col-sm-8 col-xs-12">
                                    <div class="form-group">

                                        {{Form::label('cpf','CPF')}}
                                        {{Form::text('cpf',old('cpf'),['class'=>'form-control'])}}
                                    </div>

                                    <div class="form-group">

                                        {{Form::label('nome','Nome')}}
                                        {{Form::text('nome',old('nome'),['class'=>'form-control'])}}
                                    </div>

                                    <div class="form-group">

                                        {{Form::label('dt_nasc','Data de Nascimento')}}
                                        {{Form::date('dt_nasc',old('dt_nasc'),['class'=>'form-control'])}}
                                    </div>

                                    <div class="form-group">

                                        <div id="gender" class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-default {{old('sexo') =='f'?'active':''}}"
                                                   data-toggle-class="btn-primary"
                                                   data-toggle-passive-class="btn-default">
                                                {{Form::radio('sexo','f',old('sexo') =='f'?'checked':'')}} &nbsp;
                                                Feminino &nbsp;
                                            </label>

                                            <label class="btn btn-default {{old('sexo') =='m'?'active':''}}"
                                                   data-toggle-class="btn-primary"
                                                   data-toggle-passive-class="btn-default">
                                                {{Form::radio('sexo','m',old('sexo') =='m'?'checked':'')}} &nbsp;
                                                Masculino &nbsp;
                                            </label>
                                            <br>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {{Form::label('sobre','Sobre')}}
                                        {{Form::textarea('sobre',old('sobre'),['class'=>'form-control'])}}
                                    </div>

                                </div>

                                <div class="col-md-3 col-sm-4 col-xs-12 profile_left">
                                    <div class="profile_img">
                                        <div id="crop-avatar">
                                            <!-- Current avatar -->
                                        </div>
                                        <div class="form-group">
                                            <label for="avatar_upload">Alterar imagem do Perfil</label>
                                            <input type="file" name="avatar_upload" id="avatar_upload"
                                                   class="form-control">
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-9 col-sm-8 col-xs-12">
                                    <h2 class="page-header">Especialidades</h2>
                                    {{Form::select('especialidades[]',$especialidades,old('especialidades'),['id'=>'especialidades','multiple'=>"multiple",'style'=>'width: 100%;'])}}
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="ln_solid"></div>
                                    {{Form::submit('Salvar',['class'=>'btn btn-success'])}}
                                </div>
                                {{Form::close()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@push('stylesheets')
    <link rel="stylesheet" href="{{asset('assets/css/forms.css')}}">
@endpush
@push('scripts')
    <script src="{{asset('assets/js/forms.js')}}"></script>
    <script type="text/javascript">
        $("#especialidades").select2({
            language: "pt-br"
        });
    </script>
@endpush