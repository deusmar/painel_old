@extends('layouts.blank')
@section('title','Profissionais')

@push('stylesheets')
<!-- Datatables.net -->
<link href="{{ asset("assets/css/datatables.css") }}" rel="stylesheet">
@endpush

@section('main_container')
    <div class="right_col" role="main" style="min-height: 1161px;">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Profissionais</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><a class="btn btn-info btn-sm" href="{{route('profissionais.create')}}">Adicionar</a>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        <div class="x_content">
                            <table class="table table-bordered table-striped table-responsive" id="profissionais-table">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>CPF</th>
                                    <th>Criado em</th>
                                    <th>Última atualização</th>
                                    <th>Opções</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
    <script src="{{asset('assets/js/datatables.js')}}"></script>
    @endpush

    @push('scripts')
    <script>
        $(document).ready(function () {

            $('#profissionais-table').DataTable({
                    language: {
                        url: '/assets/datatables/Portuguese-Brasil.json'
                    },
                    dom: 'Blfrtip',
                    responsive: true,
                    lengthMenu: [[10, 50, 100], [10, 50, 100]],
                    scrollCollapse: true,
                    order: [[3, "desc"]],
                    scrollY: '54vh',
                    paging: true,
                    serverSide: true,
                    ajax: '{!! route('profissionais.dataTable') !!}',
                    rowId: 'id',
                    columns: [
                        {data: 'nome', name: 'Nome', responsivePriority: 1},
                        {data: 'cpf', name: 'CPF', responsivePriority: 2},
                        {
                            data: 'created_at',
                            type: 'num',
                            render: {
                                _: 'display',
                                sort: 'timestamp'
                            },
                            responsivePriority: 4,
                        },
                        {
                            data: 'updated_at',
                            type: 'num',
                            render: {
                                _: 'display',
                                sort: 'timestamp'
                            },
                            responsivePriority: 5
                        },
                        {
                            "defaultContent": "<button class='btn btn-sm btn-default'><i class='fa fa-folder-open'></i> Abrir</button>",
                            responsivePriority: 3
                        },
                    ],
                    /* Double click*/
                    "fnDrawCallback": function () {
                        $('#profissionais-table tbody tr').dblclick(function () {
                            var redirection = $(this).attr('id');
                            document.location.href = '{{url()->current()}}/' + redirection;
                        });

                        $('#profissionais-table tbody tr').hover(function () {
                            $(this).css('cursor', 'pointer');
                        }, function () {
                            $(this).css('cursor', 'auto');
                        });
                    },
                })
                .on('click', 'button', function () {
                    document.location.href = '{{url()->current()}}/' + $(this).parents('tr').attr('id');
                });
        });
    </script>
    @endpush
@stop