@extends('layouts.blank')
@section('title', 'Editar Profissional')
@section('main_container')
    <div class="right_col" role="main" style="min-height: 1164px;">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Profissional - Editar</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Dados Pessoais</h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            {{-- FORM --}}
                            {{Form::open(['route' => ['profissionais.update',$profissional->id],'class'=>'form-horizontal form-label-left','files' => true, 'method'=>'patch'])}}
                            <div class="col-md-9 col-sm-8 col-xs-12">
                                <div class="form-group">

                                    <dt>CPF - <a href="/users/{{$profissional->cpf}}/edit"
                                                 title="Clique aqui para alterar">Alterar</a></dt>
                                    <dd>{{$profissional->cpf}}</dd>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-8 col-xs-12">
                                        {{Form::label('nome','Nome')}}
                                        {{Form::text('nome',$profissional->nome,['class'=>'form-control'])}}
                                    </div>


                                    <div class="form-group col-lg-4 col-xs-12">
                                        {{Form::label('dt_nasc','Data de Nascimento')}}
                                        {{Form::date('dt_nasc',$profissional->dt_nasc,['class'=>'form-control'])}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{--{{Form::label('sexo','Sexo')}}--}}
                                    <div id="gender" class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default {{$profissional->sexo=='f'?'active':''}}"
                                               data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                            {{Form::radio('sexo','f',$profissional->sexo=='f'?'checked':'')}} &nbsp;
                                            Feminino &nbsp;
                                        </label>

                                        <label class="btn btn-default {{$profissional->sexo=='m'?'active':''}}"
                                               data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                            {{Form::radio('sexo','m',$profissional->sexo=='m'?'checked':'')}} &nbsp;
                                            Masculino &nbsp;
                                        </label>
                                        <br>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {{Form::label('sobre','Sobre')}}
                                    {{Form::textarea('sobre',$profissional->sobre,['class'=>'form-control'])}}
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-4 col-xs-12 profile_left">
                                <div class="profile_img">
                                    <div class="row">
                                        {{--<div class="col-xs-6 col-xs-offset-3 col-sm-8 col-sm-offset-2 col-md-12 col-lg-10 col-lg-offset-2">--}}
                                        <div class="col-xs-8 col-xs-offset-2">
                                            <!-- Current avatar -->
                                            <img class="img-rounded center-block img-thumbnail"
                                                 @forelse($profissional->avatar as $avatar)
                                                 src="{{asset('/storage/images/avatars/'.$avatar->caminho)}}"
                                                 title="{{$profissional->nome}}"
                                                 @empty
                                                 src="{{asset('/assets/images/user.png')}}"
                                                 title="Foto Inexistente"
                                                 @endforelse
                                                 alt="Avatar"
                                            />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="avatar_upload">Alterar imagem do Perfil</label>
                                        <input type="file" name="avatar_upload" id="avatar_upload" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-8 col-xs-12">
                                <h2 class="page-header">Especialidades</h2>
                                {{Form::select('especialidades[]',$especialidades,$profissional->especialidades,['id'=>'especialidades','multiple'=>"multiple",'style'=>'width: 100%;'])}}
                            </div>

                            <div class="col-md-6 col-sm-8 col-xs-12">
                                <profissional-registros></profissional-registros>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="ln_solid"></div>
                                <a href="#" class="btn btn-default btn-sm" onclick="window.history.back()">
                                    <i class="fa fa-arrow-left"></i>
                                    Voltar
                                </a>
                                {{Form::button('<i class="fa fa-save"></i> Salvar',['type'=>'submit','class'=>'btn btn-success btn-sm'])}}
                            </div>
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@push('stylesheets')
    <link rel="stylesheet" href="{{asset('assets/css/forms.css')}}">
@endpush
@push('scripts')
    <script src="{{asset('assets/js/forms.js')}}"></script>
    <script type="text/javascript">
        $("#especialidades").select2({
            language: "pt-br"
        });
    </script>
    <script>
        $('#btn_conselho').on('click', function () {
            var inscricao = {
                conselho_id: $('#conselhos').val(),
                conselho: $('#conselhos option:selected').text(),
                registro: $('#conselho_registro').val(),
                uf: $('#conselho_uf').val()
            };

            $("input[name^='registro_conselho']").remove();
            var rowCount = $('#conselho_registros tbody tr').length - 2;

            $('<input>').attr('type', 'hidden').attr('name', 'registro_conselho[' + rowCount + '][conselho_id]').attr('value', inscricao.conselho_id).prependTo($('form'));
            $('<input>').attr('type', 'hidden').attr('name', 'registro_conselho[' + rowCount + '][registro]').attr('value', inscricao.registro).prependTo($('form'));
            $('<input>').attr('type', 'hidden').attr('name', 'registro_conselho[' + rowCount + '][uf]').attr('value', inscricao.uf).prependTo($('form'));

            var tr = $('<tr>');
            $('<td>').html(inscricao.conselho).appendTo(tr);
            $('<td>').html(inscricao.registro).appendTo(tr);
            $('<td>').html(inscricao.uf).appendTo(tr);

            tr.append($('<td>').html('<button class="btn btn-warning btn-remover">Remover</button>'));
            $('#conselho_registros').append(tr);
        });
    </script>
@endpush