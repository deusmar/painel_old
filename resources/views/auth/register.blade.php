@extends('auth.layouts.blank')
@section('title','Registrar')
@section('body')
	{!! Form::open(['url' => url('/register'), 'method' => 'post']) !!}

	<h1>Criar Conta</h1>

	{!! Form::text('name', old('name'), ['placeholder' => 'Nome Completo', 'class'=>'form-control']) !!}

	{!! Form::email('email', old('email'), ['placeholder' => 'Email', 'class'=>'form-control']) !!}

	{!! Form::password('password', ['placeholder' => 'Senha', 'class'=>'form-control']) !!}

	{!! Form::submit('Registrar', ['class' => 'btn btn-default']) !!}

	{!! Form::close() !!}
@stop