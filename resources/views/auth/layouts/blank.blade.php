<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Painel ClubCard | @yield('title')</title>

    <!-- Bootstrap -->
    <link href="{{ asset("assets/css/app.css") }}" rel="stylesheet">
    <!-- Font Awesome -->
    {{--<link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">--}}
    {{--<!-- Custom Theme Style -->--}}
    {{--<link href="{{ asset("css/gentelella.min.css") }}" rel="stylesheet">--}}

</head>
<body class="login">
<div class="login_wrapper">
    <div class="animate form login_form">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <section class="login_content">
            @yield('login_form')

            <div class="clearfix"></div>

            <div class="separator">
                <div class="clearfix"></div>
                <br>

                <div>
                    <h2>Club Card Saúde</h2>
                    <p>©<?= date( 'Y' )?> Todos os direitos reservados</p>
                </div>
            </div>
        </section>
    </div>
</div>
</div>
</body>
</html>