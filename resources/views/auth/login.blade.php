@extends('auth.layouts.blank')
@section('title','Login')
@section('login_form')
    {{Form::open(['url' => url('/login'), 'method' => 'post']) }}

    <h1>Entrar</h1>

    {{ Form::text('identity', old('identity'), ['placeholder' => 'Email ou CPF', 'class'=>'form-control', 'autofocus'] ) }}

    {{ Form::password('password', ['placeholder' => 'Password', 'class'=>'form-control']) }}
    <label for="remember"><input type="checkbox" name="remember" id="remember"> Remember Me</label>

    <div>
        {{ Form::submit('Log in', ['class' => 'btn btn-default submit']) }}
        <a class="reset_pass" href="{{  url('/password/reset') }}">Esqueci minha senha?</a>
    </div>
    {{ Form::close() }}
@stop
