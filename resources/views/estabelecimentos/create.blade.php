@extends('layouts.blank')
@section('title', 'Adicionar Estabelecimento')
@section('main_container')
    <div class="right_col" role="main" style="min-height: 1164px;">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Estabelecimento - Adicionar</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Dados da Empresa</h2>
                            <div class="clearfix"></div>
                        </div>

                        <div class="x_content">

                            {{-- FORM --}}
                            {{Form::open(['route' => 'estabelecimentos.store','class'=>'form-horizontal form-label-left','files' => true, 'method'=>'post'])}}

                            <div class="col-md-9 col-sm-8 col-xs-12">
                                <div class="form-group">

                                    {{Form::label('cnpj','CNPJ')}}
                                    {{Form::text('cnpj',old('cnpj'),['class'=>'form-control'])}}
                                </div>

                                <div class="form-group">

                                    {{Form::label('razao_soc','Razão Social')}}
                                    {{Form::text('razao_soc',old('razao_soc'),['class'=>'form-control'])}}
                                </div>
                                <div class="form-group">

                                    {{Form::label('nome_fan','Nome Fantasia')}}
                                    {{Form::text('nome_fan',old('nome_fan'),['class'=>'form-control'])}}
                                </div>
                                <div class="form-group">

                                    {{Form::label('tipo','Tipo(s) de Estabelecimento')}}
                                    {{Form::select('tipos[]',$tipos,old('tipos'),['id'=>'tipos','multiple'=>"multiple",'style'=>'width: 100%;'])}}
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="ln_solid"></div>
                                {{Form::submit('Salvar',['class'=>'btn btn-success'])}}
                            </div>
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@push('stylesheets')
    <link rel="stylesheet" href="{{asset('assets/css/forms.css')}}">
@endpush
@push('scripts')
<script src="{{asset('assets/js/forms.js')}}"></script>
<script type="text/javascript">
    $("#tipos").select2({
        language: "pt-br"
    });
</script>
@endpush