@extends('layouts.blank')
@section('title','Estabelecimentos')
@section('main_container')
<estabelecimento></estabelecimento>
@stop
{{--@section('main_container')--}}
{{--<div class="right_col" role="main" style="min-height: 1164px;">--}}
{{--<div class="">--}}
{{--<div class="page-title">--}}
{{--<div class="title_left">--}}
{{--<h3>Estabelecimento</h3>--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="clearfix"></div>--}}

{{--<div class="row">--}}
{{--<div class="col-md-12 col-sm-12 col-xs-12">--}}
{{--<div class="x_panel">--}}
{{--<div class="x_title">--}}
{{--<h2>{{$estabelecimento->nome_fan}}--}}
{{--<small>{{$estabelecimento->cnpj}}</small>--}}
{{--</h2>--}}
{{--<ul class="nav navbar-right panel_toolbox">--}}
{{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>--}}
{{--<li><a href="{{route('estabelecimentos.edit',$estabelecimento->id)}}"><i--}}
{{--class="fa fa-wrench"></i></a></li>--}}
{{--</ul>--}}
{{--<div class="clearfix"></div>--}}
{{--</div>--}}
{{--<div class="x_content">--}}
{{--<div class="col-md-9 col-sm-8 col-xs-12">--}}
{{--<div class="row">--}}
{{--<div class="col-md-6 col-xs-12">--}}
{{--<dl>--}}
{{--<dt>Razão Social</dt>--}}
{{--<dd>{{$estabelecimento->razao_soc}}</dd>--}}
{{--</dl>--}}
{{--</div>--}}
{{--<div class="col-md-6 col-xs-12">--}}
{{--<dl>--}}
{{--<dt>CNPJ</dt>--}}
{{--<dd>{{$estabelecimento->cnpj}}</dd>--}}
{{--</dl>--}}
{{--</div>--}}
{{--<div class="col-md-6 col-xs-12">--}}
{{--<dl>--}}
{{--<dt>Nome Fantasia</dt>--}}
{{--<dd>{{$estabelecimento->nome_fan}}</dd>--}}
{{--</dl>--}}
{{--</div>--}}
{{--<div class="col-md-6 col-xs-12">--}}
{{--<dl>--}}
{{--<dt>Tipo</dt>--}}
{{--@foreach($estabelecimento->tipos as $tipo)--}}
{{--<span class="label label-primary">{{$tipo->nome}}</span>--}}
{{--@endforeach--}}
{{--</dl>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--AVATAR--}}
{{--<div class="col-md-3 col-sm-4 col-xs-12 profile_left">--}}
{{--<div class="profile_img">--}}
{{--<div id="crop-avatar">--}}
{{--<!-- Current avatar -->--}}
{{--<img class="img-responsive avatar-view"--}}
{{--src="/storage/images/avatars/{{$estabelecimento->cnpj}}"--}}
{{--alt="Avatar"--}}
{{--title="{{$estabelecimento->nome_fan}}">--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--Especialidades--}}
{{--<div class="col-md-6 col-sm-12 col-xs-12">--}}
{{--<h2 class="page-header">--}}
{{--Unidades--}}
{{--<ul class="nav navbar-right panel_toolbox">--}}
{{--<li>--}}
{{--<a href="#" class="" data-toggle="modal" data-target=".bs-example-modal-sm">--}}
{{--<i class="fa fa-plus"></i>--}}
{{--</a>--}}
{{--</li>--}}
{{--</ul>--}}
{{--</h2>--}}

{{--@foreach($estabelecimento->unidades as $unidade)--}}
{{--<a href="{{route('unidades.show',$unidade->id)}}">--}}
{{--<span class="label label-default">{{$unidade->nome}}</span>--}}
{{--</a>--}}
{{--@endforeach--}}

{{--<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog"--}}
{{--aria-hidden="true" style="display: none;">--}}
{{--<div class="modal-dialog modal-sm">--}}
{{--<div class="modal-content">--}}

{{--<div class="modal-header">--}}
{{--<button type="button" class="close" data-dismiss="modal"--}}
{{--aria-label="Close"><span aria-hidden="true">×</span>--}}
{{--</button>--}}
{{--<h4 class="modal-title" id="myModalLabel2">Adicionar Unidade</h4>--}}
{{--</div>--}}
{{-- FORM --}}
{{--{{Form::open(['route' => 'unidades.store','class'=>'form-horizontal form-label-left','files' => true, 'method'=>'post'])}}--}}
{{--{{Form::hidden('estabelecimento_id',$estabelecimento->id)}}--}}
{{--<div class="modal-body">--}}
{{--<div class="form-group">--}}

{{--{{Form::label('nome','Nome')}}--}}
{{--{{Form::text('nome',old('nome'),['class'=>'form-control','placeholder'=>'Ex: Águas Claras'])}}--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="modal-footer">--}}
{{--<button type="button" class="btn btn-default" data-dismiss="modal">--}}
{{--Close--}}
{{--</button>--}}
{{--{{Form::submit('Salvar',['class'=>'btn btn-success'])}}--}}
{{--</div>--}}
{{--{{Form::close()}}--}}

{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

{{--Unidades--}}
{{--<div class="col-md-12 col-sm-12 col-xs-12">--}}
{{--<div class="x_panel">--}}
{{--<div class="x_title">--}}
{{--<h2>Unidades</h2>--}}
{{--<ul class="nav navbar-right panel_toolbox">--}}
{{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>--}}
{{--<li><a href="{{route('estabelecimentos.edit',$estabelecimento->id)}}"><i--}}
{{--class="fa fa-wrench"></i></a></li>--}}
{{--</ul>--}}
{{--<div class="clearfix"></div>--}}
{{--</div>--}}
{{--<div class="x_content">--}}
{{--UNIDADES--}}
{{--<div class="col-md-12 col-sm-12 col-xs-12">--}}
{{--<div class="row">--}}
{{--@foreach($estabelecimento->unidades as $unidade)--}}
{{--<div class="col-md-3 col-xs-12">--}}
{{--<div class="panel panel-default">--}}
{{--<div class="panel-heading text-center">--}}
{{--<a href="{{route('unidades.show',$unidade->id)}}">--}}
{{--{{$unidade->nome}}--}}
{{--</a>--}}
{{--</div>--}}
{{--<!-- List group -->--}}
{{--<ul class="list-group text-center">--}}
{{--@forelse($unidade->profissionais as $profissional)--}}
{{--<li class="list-group-item">--}}
{{--<a href="{{route('profissionais.show',$profissional->id)}}">--}}
{{--{{$profissional->nome}}--}}
{{--</a>--}}
{{--</li>--}}
{{--@empty--}}
{{--<li class="list-group-item">--}}
{{--Não há Profissionais Vinculados.--}}
{{--<p>--}}
{{--<a href="{{route('unidades.edit',$unidade->id)}}"--}}
{{--class="label label-warning">Clique--}}
{{--aqui--}}
{{--para editar</a></p>--}}
{{--</li>--}}
{{--@endforelse--}}
{{--</ul>--}}
{{--</div>--}}
{{--</div>--}}
{{--@endforeach--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--@stop--}}
