<?php
$array_uf = [
'AC'=>'Acre',
'AL'=>'Alagoas',
'AP'=>'Amapá',
'AM'=>'Amazonas',
'BA'=>'Bahia',
'CE'=>'Ceará',
'DF'=>'Distrito Federal',
'ES'=>'Espírito Santo',
'GO'=>'Goiás',
'MA'=>'Maranhão',
'MT'=>'Mato Grosso',
'MS'=>'Mato Grosso do Sul',
'MG'=>'Minas Gerais',
'PA'=>'Pará',
'PB'=>'Paraíba',
'PR'=>'Paraná',
'PE'=>'Pernambuco',
'PI'=>'Piauí',
'RJ'=>'Rio de Janeiro',
'RN'=>'Rio Grande do Norte',
'RS'=>'Rio Grande do Sul',
'RO'=>'Rondônia',
'RR'=>'Roraima',
'SC'=>'Santa Catarina',
'SP'=>'São Paulo',
'SE'=>'Sergipe',
'TO'=>'Tocantins'
]
?>
<div class="form-group">

    {{Form::label('cep','CEP')}}
    {{Form::text('cep',$endereco->cep,['class'=>'form-control'])}}
</div>

<div class="form-group">

    {{Form::label('logradouro','Logradouro')}}
    {{Form::text('logradouro',$endereco->logradouro,['class'=>'form-control'])}}
</div>
<div class="form-group">

    {{Form::label('numero','Número')}}
    {{Form::text('numero',$endereco->numero,['class'=>'form-control'])}}
</div>
<div class="form-group">

    {{Form::label('complemento','Complemento')}}
    {{Form::text('complemento',$endereco->complemento,['class'=>'form-control'])}}
</div>
<div class="form-group">

    {{Form::label('bairro','Bairro')}}
    {{Form::text('bairro',$endereco->bairro,['class'=>'form-control'])}}
</div>
<div class="form-group">

    {{Form::label('cidade','Cidade')}}
    {{Form::text('cidade',$endereco->cidade,['class'=>'form-control'])}}
</div>
<div class="form-group">

    {{Form::label('uf','UF')}}
    {{Form::select('uf',$array_uf,$endereco->uf,['class'=>'form-control'])}}
    {{--{{Form::text('uf',$endereco->uf,['class'=>'form-control'])}}--}}
</div>

@push('scripts')
<script type="text/javascript">

    $(document).ready(function () {

        function limpa_formulário_cep() {
            // Limpa valores do formulário de cep.
            $("#logradouro").val("");
            $("#bairro").val("");
            $("#cidade").val("");
            $("#uf").val("");
        }

        //Quando o campo cep perde o foco.
        $("#cep").blur(function () {

            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if (validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#logradouro").val("...");
                    $("#bairro").val("...");
                    $("#cidade").val("...");

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#logradouro").val(dados.logradouro);
                            $("#bairro").val(dados.bairro);
                            $("#cidade").val(dados.localidade);
                            $("#uf").val(dados.uf);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulário_cep();
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulário_cep();
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulário_cep();
            }
        });
    });

</script>
@endpush