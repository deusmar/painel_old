@extends('layouts.blank')
@section('title', 'Editar Unidade')
@section('main_container')
    <div class="right_col" role="main" style="min-height: 1164px;">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>
                        Editar Unidade
                    </h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                Dados da Unidade
                                <small>
                                    (<a href="{{route('estabelecimentos.show',$unidade->estabelecimento->id)}}">
                                        {{$unidade->estabelecimento->nome_fan}}
                                    </a>)
                                </small>
                            </h2>
                            <div class="nav navbar-right panel_toolbox">
                                {{Form::open(['route'=>['unidades.destroy',$unidade->id],'method'=>'delete'])}}
                                {{Form::button('<i class="fa fa-trash"></i>',['class'=>'btn btn-danger btn-sm','type'=>'submit'])}}
                                {{Form::close()}}
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="x_content">

                            {{-- FORM --}}
                            {{Form::model($unidade, ['route' => ['unidades.update', $unidade->id], 'method' => 'patch'])}}

                            <div class="col-md-9 col-sm-8 col-xs-12">
                                <div class="form-group">

                                    {{Form::label('nome','Nome')}}
                                    {{Form::text('nome',old('nome'),['class'=>'form-control'])}}
                                </div>

                                @include('forms.endereco.edit',['endereco'=>$unidade->endereco])

                            </div>

                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <h2 class="page-header">Corpo Clínico</h2>
                                {{Form::select('profissionais[]', $unidade->profissionais->pluck('nome','id'), $unidade->profissionais, ['id'=>'profissionais','multiple'=>"multiple",'style'=>'width: 100%;','class'=>'form-control'])}}
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="ln_solid"></div>
                                <a href="{{route('unidades.show',$unidade->id)}}" class="btn btn-default">
                                    <i class="fa fa-arrow-left"></i>
                                    Cancelar
                                </a>

                                {{Form::button('<i class="fa fa-save"></i> Salvar',['class'=>'btn btn-success','type'=>'submit'])}}
                            </div>
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@push('stylesheets')
<link rel="stylesheet" href="{{asset('assets/css/forms.css')}}">
@endpush
@push('scripts')
<script src="{{asset('assets/js/forms.js')}}"></script>
<script type="text/javascript">
    $('#profissionais').select2({
        language: "pt-BR",
        minimumInputLength: 4,
        debug: true,
        ajax: {
            url: '{{route('profissionais.index')}}',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
</script>
@endpush