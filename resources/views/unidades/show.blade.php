@extends('layouts.blank')
@section('title',$unidade->nome)
@section('main_container')
    <div class="right_col" role="main" style="min-height: 1164px;">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Unidade</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>{{$unidade->nome}}
                                <small>
                                    <a href="{{route('estabelecimentos.show',$unidade->estabelecimento->id)}}">
                                        {{$unidade->estabelecimento->nome_fan}} <i class="fa fa-external-link"></i>
                                    </a>
                                </small>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                <li><a href="{{route('unidades.edit',$unidade->id)}}"><i class="fa fa-wrench"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    @if($unidade->endereco)
                                        <dl>
                                            <dd>{{$unidade->endereco->logradouro}}, {{$unidade->endereco->numero}}</dd>
                                        </dl>
                                        @if($unidade->endereco->complemento)
                                            <dl>
                                                <dt>Complemento</dt>
                                                <dd>{{$unidade->endereco->complemento}}</dd>
                                            </dl>
                                        @endif

                                        <div class="row">
                                            <div class="col-xs-6">
                                                <dl>
                                                    <dt>Bairro</dt>
                                                    <dd>{{$unidade->endereco->bairro}}</dd>
                                                </dl>
                                            </div>
                                            <div class="col-xs-6">
                                                <dl>
                                                    <dt>Cidade</dt>
                                                    <dd>{{$unidade->endereco->cidade}} - {{$unidade->endereco->uf}}</dd>
                                                </dl>
                                            </div>

                                        </div>

                                        <dl>
                                            <dt>CEP</dt>
                                            <dd>{{$unidade->endereco->cep}}</dd>
                                        </dl>
                                    @else
                                        <dl>
                                            <dt>Endereço não cadastrado.</dt>
                                        </dl>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    {{--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4741.8520554921115!2d-48.07522358450837!3d-15.885338129431853!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a2d36d73a8ebb%3A0xd782af7b54ffefa8!2sAde+Conjunto+6+-+Samambaia+Sul%2C+Bras%C3%ADlia+-+DF!5e1!3m2!1spt-BR!2sbr!4v1506975267261"--}}
                                    {{--width="100%" height="450" frameborder="0" style="border:0"--}}
                                    {{--allowfullscreen></iframe>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{--Corpo Clínico--}}
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Corpo Clínico</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content" style="display: block;">
                            <table class="table table-hover">
                                <tbody>
                                @forelse($unidade->profissionais as $profissional)
                                    <tr>
                                        <td>
                                            <a href="{{route('profissionais.show',$profissional->id)}}">{{$profissional->nome}}</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>
                                            Nao há Profissionais vinculados
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                {{-- Contato --}}
                <contatos></contatos>
            </div>
        </div>
    </div>
    </div>
@stop