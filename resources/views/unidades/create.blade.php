@extends('layouts.blank')
@section('title', 'Adicionar Unidade')
@section('main_container')
    <div class="right_col" role="main" style="min-height: 1164px;">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Adicionar Unidade - <small>{{$estabelecimento->nome_fan}}</small></h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Dados da Unidade</h2>
                            <div class="clearfix"></div>
                        </div>

                        <div class="x_content">

                            {{-- FORM --}}
                            {{Form::open(['route' => ['unidades.store'],'class'=>'form-horizontal form-label-left','files' => true, 'method'=>'post'])}}
                            {{Form::hidden('estabelecimento_id', $estabelecimento->id)}}
                            <div class="col-md-9 col-sm-8 col-xs-12">

                                <div class="form-group">

                                    {{Form::label('nome','Nome')}}
                                    {{Form::text('nome',old('cnpj'),['class'=>'form-control','placeholder'=>'Ex: Águas Claras'])}}
                                </div>

                                @include('forms.endereco.create')
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="ln_solid"></div>
                                {{Form::submit('Salvar',['class'=>'btn btn-success'])}}
                            </div>
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@push('stylesheets')
    <link rel="stylesheet" href="{{asset('assets/css/forms.css')}}">
@endpush
@push('scripts')
    <script src="{{asset('assets/js/forms.js')}}"></script>
    <script type="text/javascript">
        $("#especialidades").select2({
            language: "pt-br"
        });
    </script>
@endpush