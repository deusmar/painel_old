<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Painel ClubCard</title>

    <!-- Bootstrap + Font Awesome + Custom Theme Style -->
    <link href="{{ asset("assets/css/app.css") }}" rel="stylesheet">
    @stack('stylesheets')

</head>

<body class="nav-md" style="">
<div class="container body">
    <div id="app"></div>
</div>
<!-- jQuery + Bootstrap + Custom Theme Scripts -->
<script src="{{ asset("assets/js/general.js")}}"></script>
<script src="{{ asset("js/app.js")}}"></script>
</body>
</html>