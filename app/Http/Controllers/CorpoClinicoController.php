<?php

namespace Painel\Http\Controllers;

use Illuminate\Http\Request;
use Mockery\Exception;
use Painel\Models\Profissional;
use Painel\Models\Unidade;

class CorpoClinicoController extends Controller
{
    /**
     * @var \Painel\Models\Profissional $profissionalModel
     * @var \Painel\Models\Unidade $unidadeModel
     */
    protected $profissionaModel,$unidadeModel;

    /**
     * CorpoClinicoController constructor.
     * @param Profissional $model
     */
    public function __construct(Profissional $profissional, Unidade $unidade)
    {
        $this->profissionaModel = $profissional;
        $this->unidadeModel = $unidade;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($unidade, $profissional)
    {
        try{
            $this->unidadeModel->detach($profissional);
            return response()->json(["unidade"=>$profissional],200);
        }catch (Exception $e){
            return response()->json('Erro ao remover. LOG:' . $e->getMessage(), 400);
        }

    }
}
