<?php

namespace Painel\Http\Controllers;

use Painel\Models\Estabelecimento;
use Illuminate\Http\Request;
use Painel\Models\Tipo;
use Painel\Models\Unidade;
use Whoops\Exception\ErrorException;

class EstabelecimentoController extends Controller
{
    protected $model, $tipoModel, $unidadeModel;

    /**
     * EstabelecimentoController constructor.
     *
     * @param \Painel\Models\Estabelecimento $model
     */
    public function __construct(Estabelecimento $model, Tipo $tipo, Unidade $unidade)
    {
        $this->model = $model;
        $this->tipoModel = $tipo;
        $this->unidadeModel = $unidade;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('estabelecimentos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory
     */
    public function create()
    {

        return view('estabelecimentos.create')
            ->with(['tipos' => $this->tipoModel->pluck('nome', 'id')]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $estabelecimento = $this->model->create($request->input());
        $estabelecimento->tipos()->attach($request->tipos);

        return redirect()->route('estabelecimentos.show', $estabelecimento->id);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory
     */
    public function show($id)
    {
        if (request()->wantsJson()) {
            $estabelecimento = $this->model->with([
                'tipos',
            ])
                ->findOrFail($id);
            return response()->json(['estabelecimento' => $estabelecimento], 200);
        }
        return view('estabelecimentos.show');
//        return view( 'estabelecimentos.show' )->with(
//            [
//                'estabelecimento' => $this->model
//                    ->with( [
//                        'tipos',
//                        'unidades.profissionais',
//                    ] )
//                    ->findOrFail( $id )
//            ]
//        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Painel\Models\Estabelecimento $estabelecimento
     *
     * @return \Illuminate\Contracts\View\Factory
     */
    public function edit($id)
    {
        return view('estabelecimentos.edit')->with(
            [
                'estabelecimento' => $this->model->findOrFail($id),
                'tipos'           => $this->tipoModel->pluck('nome', 'id'),
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Painel\Models\Estabelecimento $estabelecimento
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Estabelecimento $estabelecimento, Request $request)
    {
        try {
            $estabelecimento
                ->fill($request->input())
                ->save();
            return response()->json(['estabelecimento' => $estabelecimento], 200);
        } catch (ErrorException $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Painel\Models\Estabelecimento $estabelecimento
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Estabelecimento $estabelecimento)
    {
        //
    }

    public function dataTable()
    {
        return $this->model->datatables();
    }
}
