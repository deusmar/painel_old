<?php

namespace Painel\Http\Controllers;

use Painel\Models\Conselho;
use Illuminate\Http\Request;

class ConselhoController extends Controller
{
    protected $model;

    /**
     * ConselhoController constructor.
     * @param $model
     */
    public function __construct(Conselho $conselho)
    {
        $this->model = $conselho;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conselhos = $this->model->get(['id','nome','sigla']);

        return response()->json([
            'conselhos' => $conselhos,
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \Painel\Models\Conselho  $conselho
     * @return \Illuminate\Http\Response
     */
    public function show(Conselho $conselho)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Painel\Models\Conselho  $conselho
     * @return \Illuminate\Http\Response
     */
    public function edit(Conselho $conselho)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Painel\Models\Conselho  $conselho
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conselho $conselho)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Painel\Models\Conselho  $conselho
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conselho $conselho)
    {
        //
    }
}
