<?php

namespace Painel\Http\Controllers\Auth;

use Painel\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

	protected $login_type;
	protected $login_field = 'identity';

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

	/**
	 * Validate the user login request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return void
	 */
	protected function validateLogin(Request $request)
	{
		$this->login_type = filter_var($request->input($this->login_field), FILTER_VALIDATE_EMAIL)
			? 'email'
			: 'cpf';

		$request->merge([
			$this->login_type => $request->input($this->login_field),
		]);

		$this->validate($request, [
			$this->login_field => 'required',
			'password'         => 'required',
		]);
	}

	public function username()
	{
		return $this->login_type;
	}

	protected function sendFailedLoginResponse(Request $request)
	{
		// in case of ajax
		if ($request->wantsJson()) {
			return response()->json([
				$this->login_field => \Lang::get('auth.failed'),
			], 422);
		}

		// otherwise redirect as usual
		return redirect()->back()
		                 ->withInput()
		                 ->withErrors([
			                 $this->login_field => \Lang::get('auth.failed'),
		                 ]);
	}
}
