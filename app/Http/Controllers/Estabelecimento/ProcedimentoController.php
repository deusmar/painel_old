<?php

namespace Painel\Http\Controllers\Estabelecimento;

use Painel\Models\Procedimento;
use Illuminate\Http\Request;
use Painel\Http\Controllers\Controller;

class ProcedimentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \Painel\Models\Procedimento  $procedimento
     * @return \Illuminate\Http\Response
     */
    public function show(Procedimento $procedimento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Painel\Models\Procedimento  $procedimento
     * @return \Illuminate\Http\Response
     */
    public function edit(Procedimento $procedimento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Painel\Models\Procedimento  $procedimento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Procedimento $procedimento)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Painel\Models\Procedimento  $procedimento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Procedimento $procedimento)
    {
        //
    }
}
