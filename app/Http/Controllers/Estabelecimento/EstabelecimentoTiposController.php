<?php

namespace Painel\Http\Controllers\Estabelecimento;

use Illuminate\Http\Request;
use Painel\Http\Controllers\Controller;
use Painel\Models\Estabelecimento;
use Painel\Models\Tipo;
use Whoops\Exception\ErrorException;

class EstabelecimentoTiposController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Estabelecimento $estabelecimento, Tipo $tipo)
    {
        return response()->json(['estabelecimento_tipos' => $estabelecimento->tipos], 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Estabelecimento $estabelecimento, Tipo $tipo, Request $request)
    {
        try {
            //TODO: Melhorar este trecho, recebendo apenas ID já seria uma boa ideia
            /*
             * $tipo_list = $tipo->whereIn... é necessário para não haver injeção de dados vindos do front
             * Ex: algum ID que não esteja armazenado como Tipo de Estabelecimento...
             */
            $tipo_list = $tipo->whereIn('id',array_column($request->toArray(),'id'))->pluck('id');

            $estabelecimento->tipos()->sync($tipo_list);
            return response()->json(true,200);
        } catch (ErrorException $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

}
