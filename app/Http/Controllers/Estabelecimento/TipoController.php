<?php

namespace Painel\Http\Controllers\Estabelecimento;

use Illuminate\Http\Request;
use Painel\Http\Controllers\Controller;
use Painel\Models\Tipo;

class TipoController extends Controller
{
    protected $model;

    /**
     * TipoController constructor.
     * @param \Painel\Models\Tipo $model
     */
    public function __construct(Tipo $model)
    {
        $this->model = $model;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['tipos' => $this->model->latest()->get(['nome','id']), 200]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tipo = $this->model->create($request);
        return response()->json(['tipo' => $tipo, 200]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
