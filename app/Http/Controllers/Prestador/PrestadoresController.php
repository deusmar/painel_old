<?php

namespace Painel\Http\Controllers\Prestador;

use Painel\Models\Prestador;
use Illuminate\Http\Request;
use Painel\Http\Controllers\Controller;

class PrestadoresController extends Controller
{

    protected $model;

    /**
     * PrestadoresController constructor.
     * @param \Painel\Models\Prestador $model
     */
    public function __construct(Prestador $model)
    {
        $this->model = $model;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['prestadores' => $this->model->latest()->get()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \Painel\Models\Prestador $prestador
     * @return \Illuminate\Http\Response
     */
    public function show(Prestador $prestador)
    {
        return response()->json(['prestador' => $prestador], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Painel\Models\Prestador $prestador
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Prestador $prestador)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Painel\Models\Prestador $prestador
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prestador $prestador)
    {
        //
    }
}
