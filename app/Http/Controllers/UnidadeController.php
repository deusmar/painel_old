<?php

namespace Painel\Http\Controllers;

use Illuminate\Http\Request;
use Painel\Models\Estabelecimento;
use Painel\Models\Unidade;
use Exception;

class UnidadeController extends Controller
{
    protected $model, $estabelecimentoModel;

    /**
     * UnidadeController constructor.
     *
     * @param \Painel\Models\Unidade $model
     * @param \Painel\Models\Estabelecimento $estabelecimentoModel
     */
    public function __construct(Unidade $model, Estabelecimento $estabelecimentoModel)
    {
        $this->model = $model;
        $this->estabelecimentoModel = $estabelecimentoModel;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($estabelecimento)
    {
        try {
            if (request()->wantsJson()) {
                $unidades = $this->model->where('estabelecimento_id', '=', $estabelecimento)->get(['id', 'nome']);
                return response()->json(["lista_unidades" => $unidades], 200);
            }
            return $estabelecimento;
        } catch (Exception $e) {
            return response()->json($e, 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $estabelecimento = $this->estabelecimentoModel->find($request->estabelecimento, ['id']);

        if ($estabelecimento) {
            return view('unidades.create')->with(['estabelecimento' => $estabelecimento]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store($estabelecimento, Request $request)
    {
        if (request()->wantsJson()) {
            try {
                $unidade = $this->model->create([
                    'nome' => $request->nome,
                    'estabelecimento_id' => $estabelecimento,
                    'endereco_id' => $this->model->endereco()->create($request->input('endereco'))->id,
                ]);

                return response()->json($unidade, 201);

            } catch (Exception $e) {
                return response()->json($e, 400);
            }


        }

        $endereco_id = $this->model->endereco()->create($request->all())->id;

        $unidade = $this->model->create([
            'nome' => $request->nome,
            'estabelecimento_id' => $request->estabelecimento_id,
            'endereco_id' => $endereco_id,
        ]);

        return redirect()->route('unidades.show', $unidade->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return $unidade
     */
    public function show($id)
    {
        if (request()->wantsJson()) {
            $unidade = $this->model->with('endereco')->findOrFail($id);
            return response()->json(['unidade' => $unidade], 200);
        }
        return view('unidades.show')->with(
            [
                'unidade' => $this->model
                    ->with([
                        'estabelecimento',
                        'profissionais.avatar',
                        'endereco',
                    ])
                    ->findOrFail($id),
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\Factory
     */
    public
    function edit(
        $id
    )
    {

        return view('unidades.edit')->with(
            [
                'unidade' => $this->model->with
                (
                    ['profissionais', 'endereco']
                )
                    ->find($id),
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (request()->wantsJson())
        {
            try {
                $unidade = $this->model->findOrFail($id);
                $unidade->fill($request->all())->save();
                return response()->json(true, 200);
            }catch (Exception $e) {
                return response()->json("Não foi possível realizar a atualização: " . $e->getMessage(), 400);
            }
        }
        $unidade = $this->model->findOrFail($id);
        $unidade->fill($request->all())->save();
        $unidade->endereco->fill($request->all())->save();
        $unidade->profissionais()->sync($request->profissionais);

        return redirect()->route('unidades.show', $unidade->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Painel\Models\Estabelecimento $estabelecimento
     * @param  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Estabelecimento $estabelecimento, $id)
    {
        if (request()->wantsJson()) {
            return $estabelecimento->unidades($id)->delete();

        }
        // delete
        $unidade = $this->model->find($id);
        $unidade->delete();

        return redirect()->route('estabelecimentos.show', $unidade->estabelecimento_id);
    }
}
