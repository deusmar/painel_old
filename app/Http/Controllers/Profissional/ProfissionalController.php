<?php

namespace Painel\Http\Controllers\Profissional;

use Painel\Http\Controllers\Controller;
use Painel\Models\Avatar;
use Painel\Models\Especialidade;
use Painel\Models\Profissional;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Painel\Models\ProfissionalConselho;
use Painel\Models\User;
use Ramsey\Uuid\Uuid;

class ProfissionalController extends Controller {
	private $model, $especialidadeModel, $user, $avatarModel, $registroModel;

	public function __construct( Profissional $model, Especialidade $especialidade, User $user, Avatar $avatar, ProfissionalConselho $registro ) {
		$this->model              = $model;
		$this->especialidadeModel = $especialidade;
		$this->user               = $user;
		$this->avatarModel        = $avatar;
		$this->registroModel      = $registro;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		if ( request()->wantsJson() ) {
			$profissionais  = $this->model->where( 'nome', 'like', '%' . request()->term . '%' )->get();
			$formatted_tags = [];

			foreach ( $profissionais as $profissional ) {
				$formatted_tags[] = [ 'id' => $profissional->id, 'text' => $profissional->nome ];
			}

			return response()->json( $formatted_tags, 200 );
		}

		return view( 'profissionais.index' );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view( 'profissionais.create' )
			->with( [ 'especialidades' => $this->especialidadeModel->pluck( 'nome', 'id' ) ] );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
		$profissional = $this->model->create( $request->input() );
		if ( $request->has( 'avatar_upload' ) ) {
			$avatar   = $request->file( 'avatar_upload' );
			$filename = Uuid::uuid1() . '.' . $avatar->getClientOriginalExtension();

			Image::make( $request->file( 'avatar_upload' ) )
			     ->widen( 400, function ( $constraint ) {
				     $constraint->upsize();
			     } )
			     ->encode( 'jpg' )
			     ->save( storage_path( 'app/public/images/avatars/' ) . $filename );

			$avatar = $this->avatarModel->create( [ 'caminho' => $filename ] );
			$profissional->avatar()->sync( $avatar );
		}
		$profissional->especialidades()->sync( $request->especialidades );

		return redirect()->route( 'profissionais.edit', $profissional->id );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \Painel\Models\Profissional $profissional
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		$profissional = $this->model->with( [ 'avatar', 'especialidades', 'unidades.estabelecimento', 'credenciais' ] )->find( $id );

		return view( 'profissionais.show', compact( 'profissional' ) );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \Painel\Models\Profissional $profissional
	 *
	 * @return \Illuminate\Contracts\View\Factory
	 */
	public function edit( $id ) {
		return view( 'profissionais.edit' )
			->with(
				[
					'profissional' => $this->model->with( [ 'avatar', 'especialidades', 'credenciais' ] )->find( $id ),
				]
			);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param   $id
	 *
	 * @return \Illuminate\Http\Response
	 */
    public function update(Request $request, $id)
    {
        $profissional = $this->model->find($id);

        if ($request->has('especialidades')) {
            $profissional->especialidades()->sync($request->especialidades);
        }

        if ($request->has('avatar_upload')) {
            if ($profissional->avatar()->count()) {
                File::delete(storage_path('app/public/images/avatars/') . $profissional->avatar[0]->caminho);
                $profissional->avatar()->delete();
            }

            $filename = Uuid::uuid1() . '.' . $request->file('avatar_upload')->getClientOriginalExtension();

            Image::make($request->file('avatar_upload'))
                ->widen(400, function ($constraint) {
                    $constraint->upsize();
                })
                ->encode('jpg')
                ->save(storage_path('app/public/images/avatars/') . $filename);

            $avatar = $this->avatarModel->create(['caminho' => $filename]);
            $profissional->avatar()->sync($avatar);
        }

        $profissional->update($request->all());

        return redirect()->route('profissionais.show', $id);
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \Painel\Models\Profissional $profissional
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( Profissional $profissional ) {
		//
	}

	public function dataTable() {
		return $this->model->datatables();
	}
}
