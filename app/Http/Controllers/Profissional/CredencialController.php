<?php

namespace Painel\Http\Controllers\Profissional;

use Painel\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Painel\Models\Conselho;
use Painel\Models\Profissional;
use Painel\Models\ProfissionalConselho;

class CredencialController extends Controller
{
    protected $model, $conselhoModel;

    /**
     * CredencialController constructor.
     * @param $model
     */
    public function __construct(ProfissionalConselho $profissionalConselho, Conselho $conselhoModel)
    {
        $this->model = $profissionalConselho;
        $this->conselhoModel = $conselhoModel;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $registros = \DB::table('profissional_conselhos AS pc')
            ->where('pc.profissional_id', '=', $id)
            ->join('conselhos AS c', 'c.id', '=', 'pc.conselho_id')
            ->get(['pc.id', 'pc.registro', 'c.sigla', 'pc.uf']);

        return response()->json([
            'lista_registros' => $registros,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $profissional)
    {

        $registro = Profissional::find($profissional)->credenciais()->create([
            "conselho_id" => $request->get('conselho_id'),
            "registro"    => $request->get('registro'),
            "uf"          => $request->get('uf'),
        ]);

        return response()->json([
            'registro' => $registro,
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $profissionalId
     * @param  int $credencialId
     * @return \Illuminate\Http\Response
     */
    public function destroy($profissionalId, $credencialId)
    {
        if ($this->model->find($credencialId)->delete()) {
            return response()->json(true, 200);
        }
    }
}
