<?php

namespace Painel\Http\Controllers;

use Illuminate\Http\Request;
use Painel\Models\Unidade;
use Painel\Models\Contato;
use Whoops\Exception\ErrorException;

class UnidadeContatosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Unidade $unidade)
    {
        return response()->json(['contatos' => $unidade->contatos, 200]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Unidade $unidade, Contato $contato, Request $request)
    {
        $unidade->contatos()->attach(
            $novoContato = $contato->create([
                'tipo'   => $request->get('tipo'),
                'ddd'    => $request->get('ddd'),
                'numero' => $request->get('numero'),
                'obs'    => $request->get('obs'),
            ]));
        return response()->json(["contato" => $novoContato], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Unidade $unidade, Contato $contato, Request $request)
    {
        try {

            $contato->fill($request->all())->save();
            return response()->json('true', 200);
        } catch (ErrorException $e) {
            return response()->json(['errors' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unidade $unidade, $id)
    {
        try {
            $unidade->contatos()->detach($id);
            return response()->json($id, 200);
        } catch (ErrorException $e) {
            return response()->json(['errors'=>$e->getMessage()],400);
        }
    }
}
