<?php namespace Painel\Http\ViewComposers;

/**
 * Created by PhpStorm.
 * User: deusmar
 * Date: 15/10/17
 * Time: 17:59
 */
use Illuminate\View\View;
use Painel\Models\Conselho;
use Painel\Models\Especialidade;

class ProfissionalViewComposer
{
    /**
     * Conselho and Especialidade Model implementation.
     *
     * @var conselho
     * @var especialidade
     */
    protected $conselho,$especialidade;

    /**
     * Create a new profile composer.
     *
     * @param  Conselho $conselho
     * @param Especialidade $especialidade
     * @return void
     */
    public function __construct(Conselho $conselho, Especialidade $especialidade)
    {
        // Dependencies automatically resolved by service container...
        $this->conselho = $conselho;
        $this->especialidade= $especialidade;
    }

    public function compose(View $view)
    {
        $view->with('conselhos', $this->conselho->pluck('sigla','id'))
        ->with('especialidades',$this->especialidade->pluck( 'nome', 'id' ));
    }


}