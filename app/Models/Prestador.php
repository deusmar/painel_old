<?php

namespace Painel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Prestador extends Model
{
    use SoftDeletes;

    protected $table = 'prestadores';
    protected $fillable = [ 'nome_fantasia', 'razao_social', 'cnpj'];
    protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];
    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'cnpj';
    }
}
