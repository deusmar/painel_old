<?php

namespace Painel\Models;

use Illuminate\Database\Eloquent\Model;
use Yajra\DataTables\DataTables;

class Estabelecimento extends Model
{
    protected $table = 'estabelecimentos';
    protected $fillable = [ 'nome_fan', 'razao_soc', 'cnpj', 'matriz_id' ];
    protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];

    public function tipos()
    {
        return $this->belongsToMany( 'Painel\Models\Tipo' )->select( [ 'tipos.id', 'tipos.nome' ] );
    }

    public function unidades()
    {
        return $this->hasMany( 'Painel\Models\Unidade' );
    }

    public function datatables()
    {
        return DataTables::of( $this->query()
            ->select( [ 'estabelecimentos.id', 'estabelecimentos.nome_fan', 'estabelecimentos.cnpj', 'estabelecimentos.razao_soc', 'estabelecimentos.created_at', 'estabelecimentos.updated_at' ] )
        )
            ->editColumn( 'created_at', function ( $estabelecimentos ) {
                return [
                    'display'   => e(
                        $estabelecimentos->created_at->diffForHumans()
                    ),
                    'timestamp' => $estabelecimentos->created_at->timestamp,
                ];
            } )
            ->filterColumn( 'created_at', function ( $query, $keyword ) {
                $query->whereRaw( "DATE_FORMAT(estabelecimentos.created_at,'%m/%d/%Y') LIKE ?", [ "%$keyword%" ] );
            } )->editColumn( 'updated_at', function ( $estabelecimentos ) {
                return [
                    'display'   => e(
                        $estabelecimentos->created_at->diffForHumans()
                    ),
                    'timestamp' => $estabelecimentos->created_at->timestamp,
                ];
            } )
            ->filterColumn( 'updated_at', function ( $query, $keyword ) {
                $query->whereRaw( "DATE_FORMAT(estabelecimentos.created_at,'%m/%d/%Y') LIKE ?", [ "%$keyword%" ] );
            } )
            ->make( true );
    }
}