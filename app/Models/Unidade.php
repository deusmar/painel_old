<?php

namespace Painel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unidade extends Model
{
    use SoftDeletes;

    protected $table = 'unidades';
    protected $fillable = [ 'estabelecimento_id', 'endereco_id', 'nome' ];


    public function estabelecimento()
    {
        return $this->belongsTo( 'Painel\Models\Estabelecimento' );
    }

    public function profissionais()
    {
        return $this->belongsToMany( 'Painel\Models\Profissional', 'corpo_clinico' )
            ->select( [
                'profissionais.nome',
                'profissionais.id'
            ] );
    }

    public function contatos()
    {
        return $this->belongsToMany(Contato::class,'unidade_contatos');
    }

    public function endereco()
    {
        return $this->hasOne( 'Painel\Models\Endereco', 'id', 'endereco_id' );
    }
}
