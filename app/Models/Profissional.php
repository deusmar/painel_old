<?php

namespace Painel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yajra\DataTables\DataTables;

class Profissional extends Model
{
    use SoftDeletes;

    protected $table = 'profissionais';

    protected $primaryKey = "id";

    protected $fillable = [ 'cpf', 'nome', 'dt_nasc', 'sexo', 'sobre', 'avatar' ];

    protected $dates = [ 'dt_nasc', 'created_at', 'updated_at' ];

    public function user()
    {
        return $this->hasOne( 'Painel\Models\User', 'cpf', 'cpf' );
    }

    public function avatar()
    {
        return $this->belongsToMany( 'Painel\Models\Avatar' );
    }

    public function especialidades()
    {
        return $this->belongsToMany( 'Painel\Models\Especialidade' )->select( [
            'especialidades.id',
            'especialidades.nome'
        ] );
    }

    public function credenciais()
    {
        return $this->hasMany( ProfissionalConselho::class,'profissional_id','id' );
    }

    public function unidades()
    {
        return $this->belongsToMany( 'Painel\Models\Unidade', 'corpo_clinico' )->select( [
            'unidades.id',
            'unidades.estabelecimento_id',
            'unidades.nome'
        ] );
    }

    public function contatos()
    {
        return $this->belongsToMany( 'Painel\Models\Contato' );
    }

    public function datatables()
    {
        return DataTables::of( $this->query()
            ->select( [
                'profissionais.id',
                'profissionais.nome',
                'profissionais.cpf',
                'profissionais.created_at',
                'profissionais.updated_at'
            ] )
        )
            ->editColumn( 'created_at', function ( $profissionais ) {
                return [
                    'display'   => e(
                        $profissionais->created_at->diffForHumans()
                    ),
                    'timestamp' => $profissionais->created_at->timestamp
                ];
            } )
            ->filterColumn( 'created_at', function ( $query, $keyword ) {
                $query->whereRaw( "DATE_FORMAT(profissionais.created_at,'%m/%d/%Y') LIKE ?", [ "%$keyword%" ] );
            } )->editColumn( 'updated_at', function ( $profissionais ) {
                return [
                    'display'   => e(
                        $profissionais->created_at->diffForHumans()
                    ),
                    'timestamp' => $profissionais->created_at->timestamp
                ];
            } )
            ->filterColumn( 'updated_at', function ( $query, $keyword ) {
                $query->whereRaw( "DATE_FORMAT(profissionais.created_at,'%m/%d/%Y') LIKE ?", [ "%$keyword%" ] );
            } )
            ->make( true );
    }
}
