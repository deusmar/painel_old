<?php

namespace Painel\Models;

use Illuminate\Database\Eloquent\Model;

class Avatar extends Model
{
    protected $table = "avatares";
    protected $fillable = ['id','caminho'];
    protected $dates = ['created_at','updated_at'];

}
