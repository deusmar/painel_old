<?php

namespace Painel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Especialidade extends Model
{
	use SoftDeletes;

    protected $table = 'especialidades';
    protected $fillable = ['codigo','nome'];
    protected $dates = ['created_at','deleted_at','updated_at'];

    public function profissionais()
    {
        return $this->belongsToMany('Painel\Models\Profissional');
    }
}
