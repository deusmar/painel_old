<?php

namespace Painel\Models;

use Illuminate\Database\Eloquent\Model;

class ProfissionalConselho extends Model
{
    protected $table = 'profissional_conselhos';
    protected $primaryKey = "id";
    protected $fillable = ['profissional_id','conselho_id','registro','uf'];
    protected $dates = ['created_at', 'updated_at','deleted_at'];

    public function profissional()
    {
    	return $this->belongsTo(Profissional::class,'profissional_id','id');
    }
}
