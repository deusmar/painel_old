<?php namespace Painel\Models;

/**
 * Tipo de Estabelecimento
 */

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    protected $table = "tipos";

    protected $fillable = ["nome"];

    public function estabelecimentos()
    {
	    return $this->belongsToMany('Painel\Models\Estabelecimento');
    }
}
