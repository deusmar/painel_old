<?php

namespace Painel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Endereco extends Model {

	use SoftDeletes;

	protected $table = "enderecos";

	protected $fillable = [
		'logradouro',
		'numero',
		'complemento',
		'bairro',
		'cidade',
		'uf',
		'cep'
	];

	protected $dates = [ 'created_at', 'updated_at', 'deleted_at' ];
}
