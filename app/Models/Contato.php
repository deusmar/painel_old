<?php

namespace Painel\Models;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    protected $table = 'contatos';

    protected $fillable = ['tipo','ddd','numero','obs'];

    public function profissionais()
    {
    	return $this->belongsToMany('Painel\Models\Profissional');
    }

	public function unidades()
	{
		return $this->belongsToMany('Painel\Models\Unidade');
	}
}
