<?php

namespace Painel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conselho extends Model
{

    use SoftDeletes;

    protected $table = "conselhos";
    protected $fillable = ['sigla','nome','uf'];
    protected $dates = ['created_at','deleted_at','updated_at'];



    public function profissional()
    {
        return $this->belongsTo('Painel\Models\Profissional');
    }
}
