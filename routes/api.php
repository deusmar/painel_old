<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::namespace('Profissional')->group(function () {
    Route::resource('profissionais.conselhos', 'CredencialController')->except('post');
});

Route::namespace('Estabelecimento')->group(function ($route) {
//    $route->resource('tipos','TipoController');
//    $route->resource('estabelecimentos.tipos','EstabelecimentoTiposController');
//    $route->resource('estabelecimentos.tabelas','EstabelecimentoTabelaProcedimentos');
});

Route::resource('estabelecimentos', 'EstabelecimentoController');
Route::resource('estabelecimentos.unidades', 'UnidadeController');


Route::resource('unidades.profissionais', 'CorpoClinicoController');
Route::resource('unidades.contatos', 'UnidadeContatosController');
Route::resource('unidades', 'UnidadeController');
Route::resource('conselhos', 'ConselhoController');

Route::namespace('Prestador')->group(function ($route) {
    $route->resource('prestadores', 'PrestadoresController',
        //Parameters: Laravel trabalha c/ inglês, parameters corrige o plural "prestadore"
        ['parameters' => ['prestadores' => 'prestador',]]
    );
});

