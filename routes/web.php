<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Auth::routes();
Route::middleware(['auth'])->group(function () {
    Route::get('/', 'HomeController@index')->name('dashboard');
    Route::namespace('Profissional')->group(function () {
        Route::get('profissionais/datatables', 'ProfissionalController@dataTable')->name('profissionais.dataTable');

        Route::resource('profissionais', 'ProfissionalController');
    });

    Route::get('estabelecimentos/datatables', 'EstabelecimentoController@dataTable')->name('estabelecimentos.dataTable');

    Route::resource('estabelecimentos', 'EstabelecimentoController');

    Route::resource('unidades', 'UnidadeController');
});
Route::get('csv', function () {
    $csv = \League\Csv\Reader::createFromPath(public_path('assets/Prestadores.csv'));
    $csv->setDelimiter('#');
    $csv->setHeaderOffset(0);
    $records = $csv->getRecords();
//	dd($csv);
    $profissional = new \Painel\Models\Profissional();
//	$record = new \Illuminate\Support\Collection($record);
    foreach ($records as $record) {
        $record['cpf'] = str_replace(['-', '.', ','], "", $record['cpf']);

        if (strlen($record['cpf']) > 11 || $record['cpf'] == "") {
            $record['cpf'] = null;
        }
        //Abrevia penúltimo ou antepenúltimo sobrenome

        while (strlen($record['nome']) > 60) {

            $nome = explode(" ", $record['nome']);
            $p = 1;

            if (strlen($nome[$p]) > 3) {
                $nome[$p] = substr($nome[$p], 0, 1) . ".";
            }

        }


        if ($record['sexo'] != "m" && $record['sexo'] != "f") {

            $record['sexo'] = null;
        }

        $profissional->cpf = $record['cpf'];
        $profissional->nome = $record['nome'];
        $profissional->sexo = $record['sexo'];
        $profissional->sobre = $record['sobre'];
        $profissional->save();
    }
    echo "Fim";
});
*/

Route::get('/', function () {
    return view('index');
});
