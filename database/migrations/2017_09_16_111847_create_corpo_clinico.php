<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorpoClinico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corpo_clinico', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('unidade_id')->unsigned()->index();
	        $table->foreign('unidade_id')->references('id')->on('unidades')->onDelete('cascade');

	        $table->integer('profissional_id')->unsigned()->index();
	        $table->foreign('profissional_id')->references('id')->on('profissionais')->onDelete('cascade');

	        $table->softDeletes();
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corpo_clinico');
    }
}
