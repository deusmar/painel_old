<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnderecosTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'enderecos', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->string( 'logradouro', 40 );
			$table->string( 'numero', 6 );
			$table->string( 'complemento', 20 )->nullable();
			$table->string( 'bairro', 30 );
			$table->string( 'cidade', 30 );
			$table->char( 'uf', 2 );
			$table->string( 'cep', 8 );
			$table->timestamps();
			$table->softDeletes();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'enderecos' );
	}
}
