<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAvatarProfissional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('avatar_profissional', function (Blueprint $table) {
		    $table->integer('avatar_id')->unsigned()->index();
		    $table->foreign('avatar_id')->references('id')->on('avatares')->onDelete('cascade');

		    $table->integer('profissional_id')->unsigned()->index();
		    $table->foreign('profissional_id')->references('id')->on('profissionais')->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('avatar_profissional');
    }
}
