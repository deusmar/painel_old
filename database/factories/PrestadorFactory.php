<?php

use Faker\Generator as Faker;
use Painel\Models\Prestador as Model;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Model::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\pt_BR\Person($faker));
    $faker->addProvider(new \Faker\Provider\pt_BR\Company($faker));
    return [
        'cnpj'          => $faker->cnpj(false),
        'razao_social'  => $faker->company,
        'nome_fantasia' => $faker->company,
    ];
});
