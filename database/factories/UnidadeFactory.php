<?php

use Faker\Generator as Faker;
use Painel\Models\Unidade as Model;

$factory->define(Model::class, function (Faker $faker) {
	$faker->addProvider( new \Faker\Provider\pt_BR\Address( $faker ) );

	return [
		'nome' => $faker->city
    ];
});
