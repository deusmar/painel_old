<?php

use Faker\Generator as Faker;
use Painel\Models\Avatar as Model;

$factory->define( Model::class, function ( Faker $faker ) {
	$path = basename($faker->image( storage_path( 'app/public/images/avatars' ), 640, 480, 'people', $fullPath = true ));

	return [
		'caminho' => $path,
	];
} );