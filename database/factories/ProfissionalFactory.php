<?php

use Faker\Generator as Faker;

$factory->define( Painel\Models\Profissional::class, function ( Faker $faker ) {
	$faker->addProvider( new \Faker\Provider\pt_BR\Person( $faker ) );
	$cpf    = $faker->cpf( false );
	$gender = $faker->randomElement( [ 'male', 'female' ] );

	return [
		'nome'    => $faker->name,
		'cpf'     => $cpf,
		'sexo'    => mb_substr( $gender, 0, 1 ),
		'dt_nasc' => $faker->date( 'Y-m-d' ),
		'sobre'   => $faker->realText( 255 ),
	];
} );
