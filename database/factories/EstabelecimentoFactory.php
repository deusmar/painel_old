<?php

use Faker\Generator as Faker;

$factory->define(Painel\Models\Estabelecimento::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\pt_BR\Person($faker));
    $faker->addProvider(new \Faker\Provider\pt_BR\Company($faker));
    return [
        'cnpj'      => $faker->cnpj(false),
        'nome_fan'  => $faker->company,
        'razao_soc' => $faker->company,
    ];
});
