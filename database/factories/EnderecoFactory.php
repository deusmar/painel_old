<?php

use Faker\Generator as Faker;
use Painel\Models\Endereco as Model;

$factory->define( Model::class, function ( Faker $faker ) {
    $faker->addProvider( new \Faker\Provider\pt_BR\Address( $faker ) );

    return [
        'logradouro'  => $faker->streetName,
        'numero'      => $faker->numberBetween( 1, 500 ),
        'complemento' => $faker->secondaryAddress,
        'bairro'      => "{$faker->streetPrefix} {$faker->firstName()}",
        'cidade'      => $faker->city,
        'uf'          => $faker->stateAbbr,
        'cep'         => str_replace( '-', '', $faker->postcode )
    ];
} );
