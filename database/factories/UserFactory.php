<?php

use Faker\Generator as Faker;

$factory->define( Painel\Models\User::class, function ( Faker $faker ) {
	$faker->addProvider( new \Faker\Provider\pt_BR\Person( $faker ) );
	$cpf = $faker->cpf( false );

	return [
		'cpf'            => $cpf,
		'nome'           => $faker->name(),
		'email'          => $faker->unique()->safeEmail,
		'password'       => bcrypt( $cpf ),
		'remember_token' => str_random( 10 ),
	];
}
);
