<?php

use Illuminate\Database\Seeder;

class AdminUserTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		factory( Painel\Models\User::class, 1 )->create( [
				'cpf'            => '02409355196',
				'nome'           => 'Deusmar',
				'email'          => 'deusmar.junior@hotmail.com',
				'password'       => bcrypt( '02409355196' ),
				'remember_token' => str_random( 10 )
			]
		);

		factory( Painel\Models\User::class, 1 )->create( [
				'cpf'            => '53980514153',
				'nome'           => 'Carlos Henrique',
				'email'          => 'carloshenrique.inv@gmail.com',
				'password'       => bcrypt( '53980514153' ),
				'remember_token' => str_random( 10 )
			]
		);
	}
}
