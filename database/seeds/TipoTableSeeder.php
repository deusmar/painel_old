<?php

use Illuminate\Database\Seeder;
use Painel\Models\Tipo;

class TipoTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$tipos = [
			"PRONTO SOCORRO",
			"SERVIÇO DIAGNÓSTICO/EXAMES",
			"CONSULTÓRIOS/CLÍNICAS ESPECIALIZADAS",
			"HOSPITAL COM ATENDIMENTO AMBULATORIAL",
		];
		foreach ( $tipos as $tipo ) {
			Tipo::create(["nome"=>$tipo]);
		}

	}
}