<?php

use Illuminate\Database\Seeder;
use Painel\Models\Prestador;
class PrestadorSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Prestador::class,100)->create();
    }
}
