<?php

use Illuminate\Database\Seeder;
use Painel\Models\Profissional;
use Painel\Models\Especialidade;
use Painel\Models\Avatar;

class ProfissionalTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		factory( Profissional::class, 200 )->create()->each(
			function ( $p ) {
				$p->especialidades()->sync( Especialidade::limit( random_int( 1, 4 ) )->offset(random_int( 1, 50 ))->get() );
//				$p->avatar()->sync( factory( Avatar::class )->create()->id );
			}
		);
	}
}
