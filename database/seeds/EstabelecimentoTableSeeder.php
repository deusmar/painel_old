<?php

use Illuminate\Database\Seeder;
use Painel\Models\Estabelecimento;
use Painel\Models\Tipo;
use Painel\Models\Unidade;
use Painel\Models\Endereco;
use Painel\Models\Profissional;
use Painel\Models\Especialidade;
use Painel\Models\Avatar;
use Painel\Models\Conselho;

class EstabelecimentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos = Tipo::pluck( 'id' );
        $last = count( $tipos ) - 1;

        factory( Estabelecimento::class, 100 )->create()->each(
            function ( $e ) use ( $tipos, $last ) {
                if ( count( $tipos ) ) {
                    $e->tipos()->sync( $tipos[rand( 1, $last )] );
                }
                factory( Unidade::class, random_int( 1, 5 ) )->create(
                    [ 'estabelecimento_id' => $e->id,
                      'endereco_id'        => factory( Endereco::class )->create()->id
                    ]
                )->each(
                    function ( $u ) {
                        $u->profissionais()->sync( factory( Profissional::class, random_int( 1, 5 ) )->create()->each(
                            function ( $p ) {
                                $p->especialidades()->sync( Especialidade::limit( random_int( 1, 4 ) )->offset( random_int( 1, 50 ) )->get() );
//                                $p->avatar()->sync( factory( Avatar::class )->create()->id );
                            }
                        ) );
                    }
                );
            }
        );
    }
}
