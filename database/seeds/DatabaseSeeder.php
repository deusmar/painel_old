<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

        $this->call( AdminUserTableSeeder::class );
		$this->call( EspecialidadeTableSeeder::class );
        $this->call( TipoTableSeeder::class );
        $this->call( ConselhoTableSeeder::class );
		$this->call( EstabelecimentoTableSeeder::class );


		Model::reguard();
	}
}
