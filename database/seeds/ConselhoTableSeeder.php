<?php

use Illuminate\Database\Seeder;
use Painel\Models\Conselho;

class ConselhoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $conselhos = [
            ['sigla' => 'ABQ', 'nome' => 'Associação Brasileira de Quiropraxia'],
            ['sigla' => 'CREFITO', 'nome' => 'Conselho Regional de Fisioterapia e de Terapia Ocupacional'],
            ['sigla' => 'CREFONO', 'nome' => 'Conselho Regional de Fonoaudiologia'],
            ['sigla' => 'CRF', 'nome' => 'Conselho Regional de Farmácia'],
            ['sigla' => 'CRM', 'nome' => 'Conselho Regional de Medicina'],
            ['sigla' => 'CRN', 'nome' => 'Conselho Regional de Nutrição'],
            ['sigla' => 'CRO', 'nome' => 'Conselho Regional de Odontologia'],
            ['sigla' => 'CRP', 'nome' => 'Conselho Regional de Psicologia'],
            ['sigla' => 'Estética', 'nome' => 'Estética'],
        ];
        Conselho::insert($conselhos);
    }
}
